package org.boot.mappers;

import org.boot.entities.Transaction;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TransactionMapper implements RowMapper<Transaction> {
    @Override
    public Transaction mapRow(ResultSet rs, int rowNum) throws SQLException {
        Transaction transaction = new Transaction();
        transaction.setIdTransaction(rs.getInt("idTransaction"));
        transaction.setIdCategory(rs.getInt("idCategory"));
        transaction.setTransactionDesc(rs.getString("transactionDesc"));
        transaction.setAmount(rs.getInt("amount"));
        transaction.setCreationDate((rs.getString("creationDate")));
        return transaction;
    }
}
