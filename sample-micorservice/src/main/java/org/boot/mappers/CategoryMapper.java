package org.boot.mappers;

import org.boot.entities.Category;
import org.boot.entities.Person;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;


public class CategoryMapper implements RowMapper<Category> {

    public Category mapRow(ResultSet resultSet, int i) throws SQLException {

        Category category = new Category();
        category.setCategoryId(resultSet.getInt("idCategory"));
        category.setCategoryName(resultSet.getString("categoryDesc"));
        category.setCategoryType(resultSet.getString("categoryTypeCd"));
        category.setBudget(resultSet.getInt("budget"));
        category.setAvailableBal(resultSet.getInt("availableBalance"));
        return category;
    }
}

