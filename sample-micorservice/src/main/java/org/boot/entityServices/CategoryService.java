package org.boot.entityServices;

import org.boot.entities.Category;
import org.boot.entities.Person;
import org.boot.entities.Transaction;
import org.boot.mappers.CategoryMapper;
import org.boot.mappers.TransactionMapper;
import org.boot.service.SpringJdbcConfig;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CategoryService {

    private   DataSource dataSource;

    public CategoryService(){
        dataSource = new SpringJdbcConfig().mysqlDataSource();
    }

    public void insertCategory(Category category){

        SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(dataSource)
                .withTableName("category")
                .usingGeneratedKeyColumns("idCategory");
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("categoryDesc", category.getCategoryName());
        if(category.getCategoryType().equals("EX"))
        {
        parameters.put("budget", category.getBudget());
        }
        parameters.put("categoryTypeCd",category.getCategoryType());
        parameters.put("availableBalance",category.getBudget());
        Number id = simpleJdbcInsert.executeAndReturnKey(parameters);
        System.out.println("Generated id - " + id.longValue());
    }

     public List<Category> getCategoryListByType(String categoryType){
        if(categoryType.equals("EX")){
            SimpleJdbcCall simpleJdbcCallExpense = new SimpleJdbcCall(dataSource)
                    .withProcedureName("GetExpenseCategoryData").returningResultSet("expenseCategoryList",new CategoryMapper());
            SqlParameterSource in = new MapSqlParameterSource().addValue("categoryTypeCd",categoryType);

            Map<String, Object> out = simpleJdbcCallExpense.execute(in);

            return ((List<Category>) out.get("expenseCategoryList"));

        }
        else {
            SimpleJdbcCall simpleJdbcCallIncome = new SimpleJdbcCall(dataSource)
                    .withProcedureName("GetIncomeCategoryData").returningResultSet("incomeCategoryList",new CategoryMapper());
            SqlParameterSource in = new MapSqlParameterSource().addValue("categoryTypeCd",categoryType);

            Map<String, Object> out = simpleJdbcCallIncome.execute(in);

            return ((List<Category>) out.get("incomeCategoryList"));
        }
     }

     public static Category findCategoryById(int catId, JdbcTemplate jdbcTemplate){
         String query = "SELECT * FROM category WHERE idCategory = ?";
         System.out.println("Category id ------>" + catId);
         Category category = jdbcTemplate.queryForObject(
                 query, new Object[] { catId }, new CategoryMapper());
         return category;
     }

     public static void updateCategory(JdbcTemplate jdbcTemplate,Category category){
         Category catObj = CategoryService.findCategoryById(category.getCategoryId(),jdbcTemplate);
         int incBud = category.getBudget() - catObj.getBudget();
         int avlBalnce = catObj.getAvailableBal() + incBud;
         String SQL = "update category set categoryDesc = ? , budget =? ,availableBalance =?  where idCategory = ?";
         jdbcTemplate.update(SQL, category.getCategoryName(),category.getBudget(),avlBalnce,category.getCategoryId());
         System.out.println("Updated Record with ID = " + category.getCategoryId() );

     }

     public static  int  updateAvlBalance(JdbcTemplate jdbcTemplate,int categoryId,int transactionAmount){
         Category catObj = CategoryService.findCategoryById(categoryId,jdbcTemplate);
         int avlBalnce = 0;
         if(catObj.getCategoryType().equals("EX")) {
             System.out.println("Expense Category Available Amount");
             avlBalnce = catObj.getAvailableBal() - transactionAmount;
         }
         else {
             System.out.println("Income Category Available Amount");
             avlBalnce = catObj.getAvailableBal() + transactionAmount;
         }
         String SQL = "update category set availableBalance =?  where idCategory = ?";
         jdbcTemplate.update(SQL, avlBalnce,categoryId);
         if(avlBalnce<0){
             System.out.println("Available Balance is not sufficient");
             return -1;
         }
         System.out.println("Available Balance Reduced by = " + avlBalnce);
         System.out.println("Updated Record with ID = " + categoryId);
         return 0;
     }

     public static void deleteCategory(JdbcTemplate jdbcTemplate,Category category){
         String SQL = "delete from category where idCategory = ?";
         jdbcTemplate.update(SQL, category.getCategoryId());
         System.out.println("Deleted Record with ID = " + category.getCategoryId() );

     }

     public  List<Transaction> getTransactionsbyCatId(int CatId){
         SimpleJdbcCall simpleJdbcCallIncome = new SimpleJdbcCall(dataSource)
                 .withProcedureName("TransactionsByCatID").returningResultSet("transactionList",new TransactionMapper());
         SqlParameterSource in = new MapSqlParameterSource().addValue("categoryId",CatId);

         Map<String, Object> out = simpleJdbcCallIncome.execute(in);

         return ((List<Transaction>) out.get("transactionList"));
     }

     public int getTotalAvaialableBalanceByType(JdbcTemplate jdbc,String categoryType){

         String sql = "SELECT SUM(availableBalance) FROM category WHERE categoryTypeCd = ?";
         int sum = jdbc.queryForObject(
                 sql, new Object[] { categoryType }, Integer.class);
         return sum;
     }


}
