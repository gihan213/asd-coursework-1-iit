package org.boot.entityServices;

import org.boot.entities.Transaction;
import org.boot.mappers.TransactionMapper;
import org.boot.service.SpringJdbcConfig;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

public class TransactionService {

    public static int processCategoryTable(int catId,JdbcTemplate jdbc,int transactionAmount){
        return CategoryService.updateAvlBalance(jdbc,catId,transactionAmount);

    }

    public static boolean insertTransaction(Transaction transaction, JdbcTemplate jdbc){
        int errorReport = TransactionService.processCategoryTable( transaction.getIdCategory(),jdbc,transaction.getAmount());
        if(errorReport < 0) {
            return false;
        }
        else {
            DataSource dataSource = new SpringJdbcConfig().mysqlDataSource();
            SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(dataSource)
                    .withTableName("transaction")
                    .usingGeneratedKeyColumns("idTransaction");
            Map<String, Object> parameters = new HashMap<String, Object>();
            parameters.put("idCategory", transaction.getIdCategory());
            parameters.put("transactionDesc", transaction.getTransactionDesc());
            parameters.put("amount", transaction.getAmount());
            parameters.put("creationDate", transaction.getCreationDate());
            Number id = simpleJdbcInsert.executeAndReturnKey(parameters);
            System.out.println("Transaction Generated id - " + id.longValue());
            return true;
        }
    }
    static Transaction findTransactionById(int transactionId, JdbcTemplate jdbcTemplate){
        String query = "SELECT * FROM transaction WHERE idTransaction = ?";
        Transaction transaction = jdbcTemplate.queryForObject(
                query, new Object[] { transactionId }, new TransactionMapper());
        return transaction;
    }

    public static   boolean updateTransaction(JdbcTemplate jdbcTemplate,Transaction transaction){
        int idTransaction = transaction.getIdTransaction();
        Transaction transactionObj = findTransactionById(idTransaction,jdbcTemplate);

        int newTransactionAmountIncre = transaction.getAmount()- transactionObj.getAmount() ;
        int errorReport = TransactionService.processCategoryTable( transaction.getIdCategory(),jdbcTemplate,newTransactionAmountIncre);
        if(errorReport < 0) {
            return false;
        } else {
           String SQL = "update transaction set transactionDesc = ? , amount =? ,creationDate =?  where idTransaction = ?";
           jdbcTemplate.update(SQL, transaction.getTransactionDesc(),transaction.getAmount(),transaction.getCreationDate(),transactionObj.getIdTransaction());
           System.out.println("Updated Record with ID = " + idTransaction );
           return true;
        }
    }

    public static void deleteTransaction(JdbcTemplate jdbc,Transaction transaction){
        String SQL = "delete from transaction where idTransaction = ?";
        jdbc.update(SQL, transaction.getIdTransaction());
        System.out.println("Deleted Record with ID = " + transaction.getIdTransaction() );
    }



}
