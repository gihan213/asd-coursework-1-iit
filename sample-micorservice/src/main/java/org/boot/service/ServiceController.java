package org.boot.service;

import org.apache.log4j.Logger;
import org.boot.entities.Category;
import org.boot.entities.Transaction;
import org.boot.entityServices.CategoryService;
import org.boot.entityServices.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
public class ServiceController {
    private static Logger Log = Logger.getLogger(ServiceController.class);

    @Autowired
    JdbcTemplate jdbc;

    @RequestMapping("/index")
    public String index() {

        Log.info("Request received at index resource");
        return "Welcome";
    }
    @GetMapping(value = "/categoryType/{type}")
    public List getCategoryItems(@PathVariable("type") String catgoryType) {
        Log.info("Request recieved at ");
       List<Category> categories = new CategoryService().getCategoryListByType(catgoryType);
       return categories;
    }

    @GetMapping(value = "/Summary/{type}")
    public int getCategoryItemsToSummay(@PathVariable("type") String catgoryType) {
        Log.info("Request recieved at ");
        int sum = new CategoryService().getTotalAvaialableBalanceByType(jdbc,catgoryType);
        System.out.println(sum);
        return sum;
    }

    @PostMapping("/addnewcategory")
    public void insertCategory(Category category){
        new CategoryService().insertCategory(category);
    }

    @PostMapping("/updatecategory")
    public void updateCategory(Category category){
        CategoryService.updateCategory(jdbc,category);

    }
    @PostMapping("/deleteCategory")
    public  void deleteCategory(Category category){
        CategoryService.deleteCategory(jdbc,category);
    }

    @GetMapping(value = "categoryId/{id}")
    public Category getCategoryDetail(@PathVariable("id") int categoryId){
        return CategoryService.findCategoryById(categoryId, jdbc);
    }
    @PostMapping("/addnewtransaction")
    public void insertTransaction(Transaction transaction){
        Log.info("Request recieved at addNewTransaction");
        System.out.println("description"+transaction.getTransactionDesc());
        System.out.println("date"+transaction.getCreationDate());
        if(TransactionService.insertTransaction(transaction,jdbc)){
           Log.info("Transaction added Successfully");
        }
           Log.info("Transaction failed");

    }

    @PostMapping("/updatetransaction")
    public void updateTransaction(Transaction transaction){
        if(TransactionService.updateTransaction(jdbc,transaction)){
            Log.info("Transaction added Successfully");
        }
        Log.info("Transaction failed");
    }

    @PostMapping("/deleteTransaction")
    public  void deleteTransaction(Transaction transaction){
        TransactionService.deleteTransaction(jdbc,transaction);
    }

    @GetMapping(value = "transactionCategoryId/{id}")
    public List getTransactionsOfCategory(@PathVariable("id") int categoryId){
        Log.info("Request received for Transaction data for id: " + categoryId);
        List<Transaction> transactions= new CategoryService().getTransactionsbyCatId(categoryId);
        return transactions;
    }




}