package org.boot.entities;

import java.util.Date;

public class Category {

    private int categoryId;
    private String categoryName;
    private int budget;
    private int availableBal;
    private int userId;
    private Date startDate;
    private Date endDate;
    private String categoryType;


    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getCategoryType() {
        return categoryType;
    }

    public void setCategoryType(String categotryType) {
        this.categoryType = categotryType;
    }

    public int getBudget() {
        return budget;
    }

    public void setBudget(int budget) {
        this.budget = budget;
    }

    public int getAvailableBal() {
        return availableBal;
    }

    public void setAvailableBal(int availableBal) {
        this.availableBal = availableBal;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
}
