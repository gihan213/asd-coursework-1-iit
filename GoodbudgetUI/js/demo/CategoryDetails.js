// Set new default font family and font color to mimic Bootstrap's default styling
Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#858796';

console.log("categoryDetails.js script executed.");

function editBtn(id) {
    var hiddenInput = '<input type="hidden" name="idTransaction" value="'+id+'"/>';
    $("#transactioneditform").append(hiddenInput);
    $("#ideditTransactionModal").modal("show");
}

function deletebtn(id){
     var formdata = "&idTransaction="+id;
    $.ajax({
        url: 'http://localhost:8081/goodbudgetservice/deleteTransaction',
        type: 'POST',
        data: formdata,
        success: function(data){
            console.log("we are at success update")
        },
        error: function (xhr, ajaxOptions, thrownError) {
            var errorMsg = 'Ajax request failed: ' + xhr.responseText;
            console.log(errorMsg);
        }
    });
        location.reload();

}

$(document).ready(function(){
    var url = location.search;
    var params = new URLSearchParams(url);
    var categoryId = params.get("id");
    console.log(categoryId);

    $.ajax({
        url: "http://localhost:8081/goodbudgetservice/categoryId/"+categoryId,
        type: 'get',
        success: function(data){
            document.getElementById("idCategoryName").innerHTML = data.categoryName;
            if(data.categoryType === "EX") {
                var summary = (data.availableBal / data.budget) * 100;
                document.getElementById("idCatbudget").innerHTML = data.budget;
                document.getElementById("idCatExpenditure").innerHTML = (data.budget - data.availableBal);
                if (data.budget != 0) {
                    document.getElementById("idAvailBudgetText").innerHTML = Math.floor(summary) + "%";
                    document.getElementById("idAvailBudget").style.width = Math.floor(summary) + "%";
                }
            }
            else{
                document.getElementById("idBudgetLable").innerHTML = "Available Amount";
                document.getElementById("idCatbudget").innerHTML = data.availableBal;
                document.getElementById("idExpenditureBox").hidden = true;
                document.getElementById("idProgressBar").hidden = true;
            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            var errorMsg = 'Ajax request failed: ' + xhr.responseText;
            window.alert(errorMsg);
        }
    });

    $("#btnsubmitforedit").on("click", function (event) {
        event.preventDefault();
        validateFormCategory();
        var formData = $("#editcategoryform").serialize();
        var formObj = formData+"&categoryId="+categoryId;
        console.log(formObj);
        $.ajax({
            url: 'http://localhost:8081/goodbudgetservice/updatecategory',
            type: 'POST',
            data: formObj,
            success: function(data){
                console.log("we are at success update")

            },
            error: function (xhr, ajaxOptions, thrownError) {
                var errorMsg = 'Ajax request failed: ' + xhr.responseText;
                console.log(errorMsg);
            }
        });
        $('#ideditCategoryModal').on('hidden.bs.modal', function (e) {
            document.forms['editcategoryform'].elements['budget'].value = "" ;
            document.forms['editcategoryform'].elements['categoryName'].value ="" ;
            location.reload();
        })

    });
    $("#idDeleteCat").on("click", function (event) {

        var formObj = "&categoryId="+categoryId;
        $.ajax({
            url: 'http://localhost:8081/goodbudgetservice/deleteCategory',
            type: 'POST',
            data: formObj,
            success: function(formObj){
                console.log("we are at success update")
            },
            error: function (xhr, ajaxOptions, thrownError) {
                var errorMsg = 'Ajax request failed: ' + xhr.responseText;
                console.log(errorMsg);
            }
        });
        window.location.replace("/GoodbudgetUI/index.html");

    });


    $("#btnsubmitTransaction").on("click", function (event) {
        event.preventDefault();
        var formData = $("#transactionform").serialize();
        var formDataobj = formData+"&idCategory="+categoryId;
        console.log(formDataobj);
        $.ajax({
            url: 'http://localhost:8081/goodbudgetservice/addnewtransaction',
            type: 'POST',
            data: formDataobj,
            success: function(data){
                console.log("we are at success");
                location.reload();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                var errorMsg = 'Ajax request failed: ' + xhr.responseText;
                console.log(errorMsg);
            }
        });
        $('#idaddTransactionModal ').on('hidden.bs.modal', function (e) {
            document.forms['transactionform'].elements['amount'].value = "" ;
            document.forms['transactionform'].elements['Description'].value ="" ;
            document.forms['transactionform'].elements['date'].value ="" ;
        })
    });

    $("#btnsubmitEditedTrans").on("click", function (event) {
        event.preventDefault();

        var formData = $("#transactioneditform").serialize();
        var formDataobj = formData+"&idCategory="+categoryId;
        console.log(formDataobj);
        $.ajax({
            url: 'http://localhost:8081/goodbudgetservice/updatetransaction',
            type: 'POST',
            data: formDataobj,
            success: function(data){
                console.log("we are at success");
                location.reload();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                var errorMsg = 'Ajax request failed: ' + xhr.responseText;
                console.log(errorMsg);
            }
        });
        $('#ideditTransactionModal ').on('hidden.bs.modal', function (e) {
            document.forms['transactionform'].elements['amount'].value = "" ;
            document.forms['transactionform'].elements['Description'].value ="" ;
            document.forms['transactionform'].elements['date'].value ="" ;
        })
    });



    $.ajax({
        url: "http://localhost:8081/goodbudgetservice/transactionCategoryId/"+categoryId,
        type: 'get',
        success: function(data){
            rendorTransactionList(data);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            var errorMsg = 'Ajax request failed: ' + xhr.responseText;
            console.log(errorMsg);
        }
        }

    );


    function rendorTransactionList(data){
        var transactionsList = null;
        $.each(data,function (index,value) {

            console.log(value);
            transactionsList += "<tr>";
            transactionsList += "<td id = tdId"+value.idTransaction+">"+value.idTransaction+"</td>"
            transactionsList += "<td>"+value.transactionDesc+"</td>";
            transactionsList += "<td>"+value.amount+"</td>";
            transactionsList += "<td>"+value.creationDate+"</td>";
            transactionsList += "<td><button class='btn btn-primary' type= 'button' style='margin-right: 20px' onclick='editBtn(this.id)' id="+value.idTransaction+" >edit</button><button type = 'button' class='btn btn-danger'  onclick='deletebtn(this.id)' id="+value.idTransaction+" >delete</button></td>"
            transactionsList += "</tr>";
        });
        $("#idtransactionTable").append(transactionsList);
    }

    // Edit row on edit button click
    $(document).on("click", ".edit", function(){
        $(this).parents("tr").find("td:not(:last-child)").each(function(){
            $(this).html('<input type="text" class="form-control" value="' + $(this).text() + '">');
        });
        $(this).parents("tr").find(".add, .edit").toggle();
        $(".add-new").attr("disabled", "disabled");
    });

    function validateFormCategory() {

        var budget = document.forms['categoryform'].elements['budget'].value;
        if(budget == null || budget==""){
            document.forms['categoryform'].elements['budget'].value = 0 ;
        }
    }
});

