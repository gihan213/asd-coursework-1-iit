// Set new default font family and font color to mimic Bootstrap's default styling
Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#858796';

console.log("index.js script executed.");

$(document).ready(function getExpenses(){
      // ALL DOM RELATED FUNCTIONS GOES HERE...

    retrieveCategorySumInfo("EX");
    retrieveCategorySumInfo("IN");

    retrieveCategoryInfo("EX");
    retrieveCategoryInfo("IN");
    // since this is a click function (not a submission) validate all inputs before this function
    $("#btnsubmit").on("click", function (event) {
       event.preventDefault();
       validateForm();
       var formData = $("#categoryform").serialize();
       console.log(formData);
        $.ajax({
            url: 'http://localhost:8081/goodbudgetservice/addnewcategory',
            type: 'POST',
            data: formData,
            success: function(data){
                console.log("we are at success");
                retrieveCategoryInfo("EX");
                retrieveCategoryInfo("IN");
              },
            error: function (xhr, ajaxOptions, thrownError) {
                var errorMsg = 'Ajax request failed: ' + xhr.responseText;
                console.log(errorMsg);
            }
        });
        $('#idaddCategoryModal').on('hidden.bs.modal', function (e) {
            document.forms['categoryform'].elements['budget'].value = "" ;
            document.forms['categoryform'].elements['categoryName'].value ="" ;
        })
        location.reload();
    });

    function validateForm() {

        var budget = document.forms['categoryform'].elements['budget'].value;
        if(budget == null || budget==""){
            document.forms['categoryform'].elements['budget'].value = 0 ;
        }
    }

 });

function retrieveCategoryInfo(type) {
    $.ajax({
        url: 'http://localhost:8081/goodbudgetservice/categoryType/'+type,
        type: 'get',
        dataType: "json",
        async: false,
        success: function(data){
            renderCatrgoryList(data, type);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            var errorMsg = 'Ajax request failed: ' + xhr.responseText;
        }
    });
}

function renderCatrgoryList(data, type) {
    var categoryList = null;
    var summaryItem =null ;
    if (type === "EX"){

        categoryList = $("#idExpense");

    } else {
        categoryList = $("#idIncome");

    }
    categoryList.empty();

    $.each(data, function (index, value) {
        var listItem = "<a class=\"collapse-item\" href=\"CategoryDetails.html?id="+value.categoryId+"\">"+value.categoryName+"</a>";
        categoryList.append(listItem);
    });

}

function retrieveCategorySumInfo(type) {

    $.ajax({
        url: 'http://localhost:8081/goodbudgetservice/Summary/'+type,
        type: 'get',
        dataType: "json",
        async: false,
        success: function(data){

            renderIndexPageSummary(data, type);

        },
        error: function (xhr, ajaxOptions, thrownError) {
            var errorMsg = 'Ajax request failed: ' + xhr.responseText;
        }
    });

}

function renderIndexPageSummary(data, type) {

    var summaryItem =null ;
    if (type === "EX"){
        console.log(data);
        $("#idTotalExpense").append(data);

    } else {
        console.log(data);
        $("#idTotalEarningvalue").append(data) ;

    }

}
